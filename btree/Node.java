import java.util.*;

public class Node {
    private String element;
	private Node left;
	private Node right;
	private Node parent;
	
	public Node(){
	    this("",null,null,null);
	}
	
	public Node(String e, Node left, Node right,Node parent){
	    this.element = e;
		this.left = left;
		this.right = right;
		this.parent = parent;
	}
	
	public String element(){return element;}
	public Node left(){return left;}
	public Node right(){return right;}
	public Node parent(){return parent;}
	
	public void setElement(String s){this.element = s;}
	
	public boolean setLeft(Node x){
	    this.left = x;
		if(x != null) x.setParent(this);
		return true;
	}
	
	public boolean setRight(Node x){
	    this.right = x;
		if(x != null) x.setParent(this);
		return true;
	}
	
	public boolean setParent(Node x){this.parent = x; return true;}
	
	private int degree(){
	    int degree = 0;
		if(left != null || right != null) degree++;
		return degree;
	}
	
	public boolean isRoot(){return parent == null;}
	public boolean isLeaf(){return degree() == 0;}
	public boolean isInternal(){return degree() == 2;}
	public boolean isLeftChild(){return parent.left() == this;}
    public boolean isRightChild(){return parent.right() == this;}
    public boolean hasLeft(){return left != null;}	
	public boolean hasRight(){return right != null;}
	public String toString(){return element.toString();} 
	
	
	private Node getMin(){
	    Node x = this;
		while(x.left() != null) x = x.left();
		return x;
	}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	